using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed = 10f;
    [SerializeField] private float bulletSpeed = 100f;
    [SerializeField] private GameObject bulletPrefab;

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(horiz, 0, vert);
        direction.Normalize();

        transform.position += direction * speed * Time.deltaTime;
        transform.rotation = Quaternion.LookRotation(direction, Vector3.up);

        if(Input.GetButton("Fire1"))
        {
            GameObject go = Instantiate(bulletPrefab, transform.position, transform.rotation);
            Rigidbody rb = go.GetComponent<Rigidbody>();
            if(rb != null)
            {
                rb.velocity = bulletSpeed * transform.forward;
            }
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Ball"))
        {
            Debug.Log("Collided with ball!");
        }
    }
}
